import NProgress from 'nprogress'

const url = process.env.NODE_ENV === 'production'
  ? 'http://localhost:4000/'
  : 'http://localhost:4000/'

const http = {

  request (method, path, params) {
    NProgress.start()

    return fetch(url + path, {
      method,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
        // insert here http header
      },
      body: JSON.stringify(params)
    })
      .then(response => {
        return response.json()
      })
  },

  post (url, params) {
    return this.request('POST', url, params)
  },

  get (url, params) {
    return this.request('GET', url, params)
  },

  put (url, params) {
    return this.request('PUT', url, params)
  },

  delete (url, params) {
    return this.request('DELETE', url, params)
  },

  init () {
    // intercept here ...
  }

}

export default http
