#!/bin/bash

set -e

rm -rf node_modules/
du -sh *

if [ ${CI_COMMIT_REF_NAME} == "master" ];
then
  docker build -t registry.gitlab.com/${CI_PROJECT_PATH}:latest -t registry.gitlab.com/${CI_PROJECT_PATH}:${CI_COMMIT_SHA:0:8} .
else
  docker build -t registry.gitlab.com/${CI_PROJECT_PATH}:${CI_COMMIT_SHA:0:8} .
fi

docker login -u gitlab-ci-token -p $CI_JOB_TOKEN registry.gitlab.com
docker push registry.gitlab.com/${CI_PROJECT_PATH}
docker images
docker history registry.gitlab.com/${CI_PROJECT_PATH}:${CI_COMMIT_SHA:0:8}
